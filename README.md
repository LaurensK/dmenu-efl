Execute From List - Present a list with
[dmenu](https://tools.suckless.org/dmenu/) with aliases for a command. This is a
convenience script written in [Fish-shell](https://fishshell.com).

The aliases can be used to execute advanced commands by using wrappers for
common commands.  A number of wrappers can be found in the './wrapper'
directory.

[[_TOC_]]

## Usage

```fish
efl [OPTION]... [LISTFILE]
```

OPTIONS:

* -h, --help  -> outputs this helptext
* -p, --prompt -> dmenu prompt string, defaults to 'Execute [LISTFILE]:'.
* -f, --flags -> dmenu flags [bfinv]. See 'man dmenu' for more information.
* -l, --lines -> dmenu number of lines for vertical mode.

This script is designed to be run from a keybinding. i3 Example:

```sh
bindsym $mod+F1 exec --no-startup-id efl -f in -p "Default:" default
```

## LISTFILE lookup order

1. $PWD/LISTFILE
2. $XDG_CONFIG_HOME/dmenu-efl/LISTFILE

## LISTFILE format

The listfile must have an extension '.list', otherwise it wont be found.  The
listfile must contain at least 1 comma separated row.

Useable columns:

1. 'Name' - the name that should be presented in dmenu
2. 'Command' - the command that should be executed if this name is selected from dmenu

A third column is optional. If 'true' the command is interpreted as a terminal
command. If 'false' (default if omitted) the command is executed 'as it is'.
To use the terminal command option the variable '$TERMINAL' must be set and the
terminal must support the '-e' option to execute a command directly.

Lines starting with a '#' are ommitted as a comment line.

EXAMPLE:

```text
#This is a comment
#Name,Command,isTerminalApp
Vifm File Manager,vifm,true
```

## Install - Uninstall

1. Make sure you have 'fish' installed: `which fish`
1. Make sure you have 'dmenu' installed: `which dmenu`
1. Clone the repository
1. Configure value of USER_SCRIPTS_PATH in script 'install'
1. Run from repository location:

```fish
./install
```

This will spawn a dmenu that gives you the option to install or uninstall the
scripts from USER_SCRIPTS_PATH. After installing and the USER_SCRIPTS_PATH is
available from your '$PATH' variable you can call efl from anywhere by just
typing the `efl` command.

The installer asks you to choose between adding a symlink or just copy the
script. This simply depends on the users preference.

Run below command from repository directory to verify if installation was
successful:

```fish
efl example
```

## Wrappers

From this repository a number of usable example wrappers is available.
Please create a pull request for other interesting wrappers.

### Example: Pamac_wrapper

You don't always want to open a terminal first or boot up a GUI package
manager.  Just calling 'pamac checkupdates' from a listfile won't keep the
terminal open.  If you use a wrapper you can add user interaction and keep the
terminal open as long as is needed. To use the 'pamac_wrapper' the listfile
could look like this:

```text
#number Name,Command,isTerminalApp
1 Check for updates,pamac_wrapper,true
2 Pamac Manager,pamac-manager,false
```

### Other available wrappers

| wrapper         | description                                                               |
| ---             | ---                                                                       |
| qb_wrapper      | Start 'Qutebrowser' with hardware acceleration flags                      |
| sensors_wrapper | Pipe 'Sensors' output into 'less' to keep terminal open                   |
| vpn_wrapper     | Toggles the selected VPN name on/off. Based on 'nmcli'                    |
| xrandr_wrapper  | Runs some additional commands after running an Xrandr screenlayout script |
